import { Injectable } from 'angular2/core';
import { Http, Response, Headers, RequestOptions} from 'angular2/http';
import { Observable } from 'rxjs/Observable';

import { CONFIG } from '../config'
import { Meeting } from './meeting';

@Injectable()
export class MeetingService {

    private _meetingUrl = CONFIG.baseUrls.meeting;

    constructor(private _http: Http) { }

    getMeetings(): Observable<Meeting[]> {
        return this._http.get(this._meetingUrl)
            .map((response: Response) => <Meeting[]>response.json())
            .do(data => console.log(JSON.stringify(data)))
            .catch(this.handleError);
    }

    getMeeting(id: number): Observable<Meeting> {
        return this._http.get(`${this._meetingUrl}/${id}`)
            .map((response: Response) => <Meeting>response.json())
            .do(data => console.log(JSON.stringify(data)))
            .catch(this.handleError);
    }
    createMeeting(meeting: Meeting) {

        let body = JSON.stringify(meeting);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._meetingUrl, body, options)
            .map((response: Response) => <Meeting[]>response.json())
            .do(data => console.log(JSON.stringify(data)))
            .catch(this.handleError);
    }

    isStringEmpty(string: String): boolean {
        if (typeof string == 'undefined') {
            return true;
        }
        else {
            if (string.trim().length == 0) {
                return true;
            }
            else {
                return false;
            }
        }

    }

    isValidPassword(password: String): Boolean {
        if (typeof password == 'undefined') {
            return false;
        }
        else {
            if (password.trim().length < 6) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    private handleError(error: Response) {
        return Observable.throw(error.json().error || 'ERROR');
    }
}