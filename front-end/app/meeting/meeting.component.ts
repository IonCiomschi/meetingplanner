import { Component, OnInit, Input, OnChanges }  from 'angular2/core';
import { RouteParams, Router, ROUTER_DIRECTIVES} from 'angular2/router';

import { Meeting } from './meeting';
import { MeetingService } from './meeting.service';


@Component({
    templateUrl: 'app/meeting/meeting.component.html',
    directives: [ROUTER_DIRECTIVES],
})

export class MeetingComponent {
    meetings: Meeting[];
    meeting = new Meeting();
    errorMessage: string;
    buttonName: String = 'Create Meeting';
    isClicked: Boolean = false;

    constructor(
        private _meetingService: MeetingService
    ) { }

    ngOnInit(): void {
        this.getMeetings();
    }
    getMeetings(): void {
        this._meetingService.getMeetings()
            .subscribe(
            response => this.meetings = response,
            error => this.errorMessage = <any>error);
    }

    saveMeeting(): void {
        this._meetingService.createMeeting(this.meeting)
            .subscribe(
            error => this.errorMessage = <any>error);
        this.changeButtonStatus();
    }

    changeButtonStatus(): void {
        this.isClicked = !this.isClicked
        if (this.isClicked) {
            this.buttonName = 'Hide Form';
        }
        else {
            this.buttonName = 'Create Meeting';
        }
    }
}