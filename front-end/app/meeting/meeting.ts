export class Meeting {
    _id: number;
    userName: string;
    objective: string;
    startTime: string;
    endTime: string;
}