import { Component } from 'angular2/core';
import { HTTP_PROVIDERS } from 'angular2/http';
import 'rxjs/Rx';
import { ROUTER_PROVIDERS, RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';

import { UserService } from './authentication/user.service';
import { MeetingService } from './meeting/meeting.service';

import { RegisterComponent } from './authentication/register.component';
import { LoginComponent } from './authentication/login.component';

import { MeetingComponent } from './meeting/meeting.component';

@Component({
    selector: 'meeting-planner',
    templateUrl: 'app/app.component.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [
        MeetingService,
        UserService,
        HTTP_PROVIDERS,
        ROUTER_PROVIDERS
    ]
})

@RouteConfig([
    {
        path: '/login',
        name: 'Login',
        component: LoginComponent
    },
    {
        path: '/register',
        name: 'Register',
        component: RegisterComponent
    },
    {
        path: '/meeting',
        name: 'Meetings',
        component: MeetingComponent
    }
])

export class AppComponent {
}