import { Component, OnInit, Input, OnChanges }  from 'angular2/core';
import { RouteParams, Router, ROUTER_DIRECTIVES} from 'angular2/router';

import { User } from './user';
import { UserService } from './user.service';


@Component({
    templateUrl: 'app/authentication/register.component.html',
    directives: [ROUTER_DIRECTIVES],
})

export class RegisterComponent {
    user = new User();
    serverErrorMessage: String;
    errorMessage: String[] = [];
    validUser = false;
    constructor(
        private _userService: UserService,
        private _routeParams: RouteParams
    ) { }


    saveUser(): void {
        let userValid = this.userValidation(this.user);
        if (userValid) {
            this._userService.createUSer(this.user)
                .subscribe(
                error => this.serverErrorMessage = <any>error);
        }
    }

    private userValidation(user: User): Boolean {
        if (this._userService.isStringEmpty(user.firstName)) {
            this.errorMessage.push('First Name is empty');
            return false;
        }
        else if (this._userService.isStringEmpty(user.lastName)) {
            this.errorMessage.pop();
            this.errorMessage.push('Last Name is empty');
            return false;
        }
        else if (this._userService.isStringEmpty(user.email)) {
            this.errorMessage.pop();
            this.errorMessage.push('Email is empty');
            return false;
        }
        else if (!this._userService.isValidPassword(user.password)) {
            this.errorMessage.pop();
            this.errorMessage.push('Password is empty or is less then 6 characters');
            return false;
        }
        else {
            this.errorMessage.pop();
            this.validUser = true;
            return true;
        }
    }
}