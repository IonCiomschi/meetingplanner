import { Component, OnInit, Input, OnChanges }  from 'angular2/core';
import { RouteParams, Router, ROUTER_DIRECTIVES} from 'angular2/router';

import { User } from './user';
import { UserService } from './user.service';


@Component({
    templateUrl: 'app/authentication/login.component.html',
    directives: [ROUTER_DIRECTIVES],
})

export class LoginComponent {
    user = new User();
    logedUser = new User();
    serverErrorMessage: String;
    errorMessage: String[] = [];
    validUser = false;

    constructor(
        private _userService: UserService
    ) { }

    login(): void {
        let userValid = this.inputDataValidation(this.user);
        if (userValid) {
            this.getLogedUser();
            //validate input password with daba base
            this.validUser = true;
        }
    }

    private inputDataValidation(user: User): Boolean {
        if (this._userService.isStringEmpty(user.email)) {
            this.errorMessage.pop();
            this.errorMessage.push('Email is empty');
            return false;
        }
        else if (!this._userService.isValidPassword(user.password)) {
            this.errorMessage.pop();
            this.errorMessage.push('Password is empty or is less then 6 characters');
            return false;
        }
        else {
            this.errorMessage.pop();
            return true;
        }
    }

    private getLogedUser(): void {
        this._userService.getUser(this.user.email)
            .subscribe(
            logedUser => this.logedUser = logedUser,
            error => this.errorMessage = <any>error);
    }
}