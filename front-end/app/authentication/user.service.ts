import { Injectable } from 'angular2/core';
import { Http, Response, Headers, RequestOptions} from 'angular2/http';
import { Observable } from 'rxjs/Observable';

import { CONFIG } from '../config'
import { User } from './user';

@Injectable()
export class UserService {

    private _userUrl = CONFIG.baseUrls.user;

    constructor(private _http: Http) { }

    getUsers(): Observable<User[]> {
        return this._http.get(this._userUrl)
            .map((response: Response) => <User[]>response.json())
            .do(data => console.log(JSON.stringify(data)))
            .catch(this.handleError);
    }

    getUser(email: string): Observable<User> {
        return this._http.get(`${this._userUrl}/${email}`)
            .map((response: Response) => <User>response.json())
            .do(data => console.log(JSON.stringify(data)))
            .catch(this.handleError);
    }

    createUSer(user: User) {

        let body = JSON.stringify(user);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._userUrl, body, options)
            .map((response: Response) => <User[]>response.json())
            .do(data => console.log(JSON.stringify(data)))
            .catch(this.handleError);
    }

    isStringEmpty(string: String): boolean {
        if (typeof string == 'undefined') {
            return true;
        }
        else {
            if (string.trim().length == 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    isValidPassword(password: String): Boolean {
        if (typeof password == 'undefined') {
            return false;
        }
        else {
            if (password.trim().length < 6) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    private handleError(error: Response) {
        return Observable.throw(error.json().error || 'ERROR');
    }
}