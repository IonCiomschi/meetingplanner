

1. Clone the repo:


git clone https://IonCiomschi@bitbucket.org/IonCiomschi/meetingplanner.git

2.Install server dependencies and run the server: (localhost:2000)


	cd back-end
	npm install
	npm start

3.Install client dependencies and run the client: (localhost:3000)


	cd front-end
	npm install
	npm start

Additional Information and References:

Back-End:


	-Node.js + MongoDB
	-data base is located on mlab.com

Front-End:


        -Angular2 + TypeScript + Bootstrap