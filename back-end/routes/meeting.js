var express = require('express');
var router = express.Router();
var meetingController = require('../controller/meeting.server.controller')

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

router.post('/meeting', function (req, res) {
    return meetingController.createMeeting(req, res);
})

router.get('/meeting', function (req, res) {
    return meetingController.getMeetings(req, res);
})
router.get('/meeting/:id', function (req, res) {
    return meetingController.getMeeting(req, res);
})

module.exports = router;
