var express = require('express');
var router = express.Router();
var userController = require('../controller/user.server.controller')

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

router.post('/user', function (req, res) {
  return userController.createUser(req, res);
})

router.get('/user', function (req, res) {
  return userController.getUsers(req, res);
})
router.get('/user/:email', function (req, res) {
  return userController.getUser(req, res);
})

module.exports = router;
