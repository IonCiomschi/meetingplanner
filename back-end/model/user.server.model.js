var mangoose = require('mongoose');
var Schema = mangoose.Schema;

var sizeValidator = [
    function (val) {
        return (val.length >= 6)
    },
    'Password is too short (minimum is 6 characters)'
];

var requieredStringValidator = [
    function (val) {
        var auxVal = val.trim();
        return (auxVal.length > 0)
    },
    '{PATH} cannot be empty!'
];

var userSchema = new Schema({
    firstName: { type: String, required: true, validate: requieredStringValidator },
    lastName: { type: String, required: true, validate: requieredStringValidator },
    email: { type: String, required: true, validate: requieredStringValidator },
    password: { type: String, required: true, validate: sizeValidator }
});

module.exports = mangoose.model('User', userSchema);