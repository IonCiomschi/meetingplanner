var mangoose = require('mongoose');
var Schema = mangoose.Schema;

var requieredStringValidator = [
    function (val) {
        var auxVal = val.trim();
        return (auxVal.length > 0)
    },
    '{PATH} cannot be empty!'
];

var meetingSchema = new Schema({
    userName: { type: String, required: true, validate: requieredStringValidator },
    objective: { type: String, required: true, validate: requieredStringValidator },
    startTime: { type: String, required: true, validate: requieredStringValidator },
    endTime: { type: String, required: true, validate: requieredStringValidator }
});

module.exports = mangoose.model('Meeting', meetingSchema);