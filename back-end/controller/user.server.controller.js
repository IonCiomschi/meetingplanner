var User = require('../model/user.server.model.js')

exports.createUser = function (req, res) {
    var entry = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password
    });
    entry.save(function (err) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            console.log('Saved!');
            res.json(entry);
        }
    });

}

exports.getUsers = function (req, res) {
    User.find({}).exec(function (err, users) {
        if (err) {
            res.send('Error to GET Users ', +err);
        }
        else {
            res.json(users);
        }
    })
}

exports.getUser = function (req, res) {
    var email = req.params.email;
    User.findOne({ email: email }).exec(function (err, user) {
        if (err) {
            res.send('Error to GET User ', +err);
        }
        else {
            res.json(user);
        }
    })
}