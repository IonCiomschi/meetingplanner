var User = require('../model/meeting.server.model.js')

exports.createMeeting = function (req, res) {
    var entry = new User({
        userName: req.body.userName,
        objective: req.body.objective,
        startTime: req.body.startTime,
        endTime: req.body.endTime
    });
    entry.save(function (err) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            console.log('Saved!');
            res.json(entry);
        }
    });

}

exports.getMeetings = function (req, res) {
    User.find({}).exec(function (err, meetings) {
        if (err) {
            res.send('Error to GET meetings ', +err);
        }
        else {
            res.json(meetings);
        }
    })
}

exports.getMeeting = function (req, res) {
    var id = req.params.id;
    User.findOne({ _id: id }).exec(function (err, user) {
        if (err) {
            res.send('Error to GET User ', +err);
        }
        else {
            res.json(user);
        }
    })
}